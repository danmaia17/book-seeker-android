# book-seeker-android

Olá,
Projeto entregue com as seguintes tecnologias/libs:

- Kotlin
- Arquitetura MVVM
- Courotines
- Room
- Constraint Layout
- Retrofit
- JUnit
- Mockito
- Life cycle
- RecyclerView

entre outras..

Infelizmente eu já tinha alguns compromissos no fim de semana e só conseguir atuar no projeto sexta a noite, e na tarde/noite/madrugada de domingo. Sendo assim, finalizei as tarefas que achava mais importantes como:

- Tela de listagem e detalhes de livros
- Salvando dados no banco de dados permitindo usar o app offline

Não consegui implementar a tela de pesquisa de livros porque finalizei já era na madrugada de segunda e tinha compromisso no meu trabalho na segunda bem cedo, se for o caso de aumentar o prazo, posso desenvolver esta tela, mas como acredito que o objetivo dela era mais mostrar os dados salvos em um banco de dados, acredito que já consegui mostrar esta funcionalidade na tela de listagem.

Sendo assim, segue o projeto finalizado com os requisitos que consegui entregar no prazo que tinha disponível, estou disponível para eventuais dúvidas e aguardo um retorno.

Obrigado.

