package com.ciandt.book.seeker

import android.app.Application
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ciandt.book.seeker.api.OperationCallback
import com.ciandt.book.seeker.model.Book
import com.ciandt.book.seeker.repository.BookDataSource
import com.ciandt.book.seeker.viewmodel.book.BookViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.*

class MVVMTest {

    @Mock
    private lateinit var repository: BookDataSource
    @Mock
    private lateinit var context: Application

    @Captor
    private lateinit var operationCallbackCaptor: ArgumentCaptor<OperationCallback>

    private lateinit var viewModel: BookViewModel

    private lateinit var isViewLoadingObserver: Observer<Boolean>
    private lateinit var onMessageErrorObserver: Observer<Any>
    private lateinit var emptyListObserver: Observer<Boolean>
    private lateinit var onRenderBooksObserver: Observer<List<Book>>

    private lateinit var bookEmptyList:List<Book>
    private lateinit var bookList:List<Book>

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`<Context>(context.applicationContext).thenReturn(context)

        viewModel= BookViewModel(repository)

        mockData()
        setupObservers()
    }

    @Test
    fun BookEmptyListRepositoryAndViewModel(){
        with(viewModel){
            getBooks()
            isViewLoading.observeForever(isViewLoadingObserver)
            isEmptyList.observeForever(emptyListObserver)
            books.observeForever(onRenderBooksObserver)
        }

        Mockito.verify(repository, Mockito.times(1)).retrieveBooks(capture(operationCallbackCaptor))
        operationCallbackCaptor.value.onSuccess(bookEmptyList)

        Assert.assertNotNull(viewModel.isViewLoading.value)
        Assert.assertTrue(viewModel.isEmptyList.value==true)
        Assert.assertTrue(viewModel.books.value?.size==0)
    }

    @Test
    fun BookListRepositoryAndViewModel(){
        with(viewModel){
            getBooks()
            isViewLoading.observeForever(isViewLoadingObserver)
            books.observeForever(onRenderBooksObserver)
        }

        Mockito.verify(repository, Mockito.times(1)).retrieveBooks(capture(operationCallbackCaptor))
        operationCallbackCaptor.value.onSuccess(bookList)

        Assert.assertNotNull(viewModel.isViewLoading.value)
        Assert.assertTrue(viewModel.books.value?.size==3)
    }

    @Test
    fun BookFailRepositoryAndViewModel(){
        with(viewModel){
            getBooks()
            isViewLoading.observeForever(isViewLoadingObserver)
            onMessageError.observeForever(onMessageErrorObserver)
        }
        Mockito.verify(repository, Mockito.times(1)).retrieveBooks(capture(operationCallbackCaptor))
        operationCallbackCaptor.value.onError("error")
        Assert.assertNotNull(viewModel.isViewLoading.value)
        Assert.assertNotNull(viewModel.onMessageError.value)
    }

    private fun setupObservers(){
        isViewLoadingObserver= Mockito.mock(Observer::class.java) as Observer<Boolean>
        onMessageErrorObserver= Mockito.mock(Observer::class.java) as Observer<Any>
        emptyListObserver= Mockito.mock(Observer::class.java) as Observer<Boolean>
        onRenderBooksObserver= Mockito.mock(Observer::class.java) as Observer<List<Book>>
    }

    private fun mockData(){
        bookEmptyList= emptyList()
        val mockList:MutableList<Book>  = mutableListOf()
        mockList.add(Book(0,"Book 1","owner 1","Description Book 1", "url", "photo_url"))
        mockList.add(Book(1,"Book 2","owner 1","Description Book 2", "url","photo_url"))
        mockList.add(Book(2,"Book 3","owner 1","Description Book 3", "url","photo_url"))

        bookList= mockList.toList()
    }
}