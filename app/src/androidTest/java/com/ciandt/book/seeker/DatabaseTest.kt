package com.ciandt.book.seeker

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.ciandt.book.seeker.database.AppDatabase
import com.ciandt.book.seeker.model.Book
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DatabaseTest {

    private var db: AppDatabase? = null

    @Before
    fun createDb() {
        db = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getTargetContext(),
            AppDatabase::class.java!!
        ).build()
    }

    @After
    fun closeDb() {
        db?.close()
    }

    @Test
    fun insertAndDeleteBookTest() {
        val book: Book = getBook()
        testInsertBook(book)
        testDeleteBook()
    }

    private fun getBook() : Book {
        return Book(1,
            "Book 1",
            "owner 1",
            "Description Book 1",
            "url",
            "photo_url")
    }

    private fun testInsertBook(b: Book) {
        db?.bookDao()?.insert(b)
        val book = db?.bookDao()?.getBookByTrackId(1)
        Assert.assertEquals(b.trackId, book?.trackId)
    }

    private fun testDeleteBook() {
        db?.bookDao()?.deleteBooks()
        val books : List<Book>? = db?.bookDao()?.getAllBooks()
        Assert.assertEquals(books?.size, 0)
    }
}