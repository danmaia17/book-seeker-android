package com.ciandt.book.seeker.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Book(@PrimaryKey val trackId:Int,
                val trackName: String,
                val artistName: String,
                val description: String,
                val trackViewUrl: String,
                val artworkUrl100: String): Serializable