package com.ciandt.book.seeker.di

import com.ciandt.book.seeker.repository.BookDataSource
import com.ciandt.book.seeker.repository.BookRepository

object Injection {
    fun providerRepository(): BookDataSource {
        return BookRepository()
    }
}