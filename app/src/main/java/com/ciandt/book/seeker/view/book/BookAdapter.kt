package com.ciandt.book.seeker.view.book.book

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.model.Book

class BookAdapter(private var books:List<Book>, val listener: OnItemClickListener): RecyclerView.Adapter<BookAdapter.MViewHolder>(){

    interface OnItemClickListener {
        fun onItemClicked(trackId: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
        return MViewHolder(view)
    }

    override fun onBindViewHolder(vh: MViewHolder, position: Int) {
        val book= books[position]

        Glide.with(vh.imgAvatar.context).load(book.artworkUrl100).into(vh.imgAvatar)
        vh.txtName.text= book.trackName
        vh.txtArtists.text = book.artistName

        vh?.view.setOnClickListener {
            listener.onItemClicked(book.trackId)
        }
    }

    override fun getItemCount(): Int {
        return books.size
    }

    fun update(data:List<Book>){
        books= data
        notifyDataSetChanged()
    }

    class MViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        val imgAvatar: ImageView = view.findViewById(R.id.imgAvatar)
        val txtName: TextView = view.findViewById(R.id.txtName)
        val txtArtists: TextView = view.findViewById(R.id.txtArtist)
    }
}