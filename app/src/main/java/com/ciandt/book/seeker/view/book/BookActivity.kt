package com.ciandt.book.seeker.view.book

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ciandt.book.seeker.App
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.databinding.ActivityBookBinding
import com.ciandt.book.seeker.di.Injection
import com.ciandt.book.seeker.model.Book
import com.ciandt.book.seeker.util.Util
import com.ciandt.book.seeker.view.book.book.BookAdapter
import com.ciandt.book.seeker.view.detail.DetailActivity
import com.ciandt.book.seeker.viewmodel.book.BookViewModel
import com.ciandt.book.seeker.viewmodel.book.BookViewModelFactory
import kotlinx.android.synthetic.main.activity_book.*
import org.jetbrains.anko.longToast

class BookActivity : AppCompatActivity(), BookAdapter.OnItemClickListener {

    private lateinit var binding: ActivityBookBinding
    private lateinit var viewModel: BookViewModel
    private lateinit var adapter: BookAdapter

    companion object {
        const val TAG= "BookActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_book)

        initViewModel()
        initUI()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBooks()
    }

    private fun initUI(){
        adapter= BookAdapter(
            viewModel.books.value ?: emptyList(),
            this
        )
        recyclerView.layoutManager= LinearLayoutManager(this)
        recyclerView.adapter= adapter
    }

    private fun initViewModel(){
        viewModel = ViewModelProviders.of(this, BookViewModelFactory(Injection.providerRepository())).get(BookViewModel::class.java)
        viewModel.books.observe(this, renderBooks)

        viewModel.isViewLoading.observe(this,isViewLoadingObserver)
        viewModel.onMessageError.observe(this,onMessageErrorObserver)
        viewModel.isEmptyList.observe(this,emptyListObserver)
        viewModel.booksDb.observe(this, renderBooksOnDb)
    }

    private val renderBooksOnDb= Observer<List<Book>> {
        Log.v(TAG, "data updated $it")

        if (it.isEmpty()) {
            Log.v(TAG, "emptyListObserver $it")
            layoutEmpty.visibility= View.VISIBLE
            layoutError.visibility= View.GONE
        } else {
            layoutError.visibility = View.GONE
            layoutEmpty.visibility = View.GONE
            adapter.update(it)
        }
    }

    private val renderBooks= Observer<List<Book>> {
        Log.v(TAG, "data updated $it")
        layoutError.visibility= View.GONE
        layoutEmpty.visibility= View.GONE
        adapter.update(it)
    }

    private val isViewLoadingObserver= Observer<Boolean> {
        Log.v(TAG, "isViewLoading $it")
        val visibility=if(it) View.VISIBLE else View.GONE
        progressBar.visibility= visibility
    }

    private val onMessageErrorObserver= Observer<Any> {
        longToast(R.string.activity_books_error)
        viewModel.getBooksOnDb()
    }

    private val emptyListObserver= Observer<Boolean> {
        viewModel.getBooksOnDb()
    }

    override fun onItemClicked(trackId: Int) {
        if (Util.isOnline(App.applicationContext())) {
            val intent = Intent(App.applicationContext(), DetailActivity::class.java)
            intent.putExtra("param_id", trackId)
            startActivity(intent)
        } else
            longToast(R.string.activity_books_no_internet)
    }
}
