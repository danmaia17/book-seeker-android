package com.ciandt.book.seeker.api

interface OperationCallback {
    fun onSuccess(obj:Any?)
    fun onError(obj:Any?)
}