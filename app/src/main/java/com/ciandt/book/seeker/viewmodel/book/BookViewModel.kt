package com.ciandt.book.seeker.viewmodel.book

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ciandt.book.seeker.api.OperationCallback
import com.ciandt.book.seeker.model.Book
import com.ciandt.book.seeker.repository.BookDataSource
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class BookViewModel(private val repository: BookDataSource): ViewModel() {

    private val _books = MutableLiveData<List<Book>>().apply { value = emptyList() }
    val books: LiveData<List<Book>> = _books

    private val _isViewLoading= MutableLiveData<Boolean>()
    val isViewLoading: LiveData<Boolean> = _isViewLoading

    private val _onMessageError= MutableLiveData<Any>()
    val onMessageError: LiveData<Any> = _onMessageError

    private val _isEmptyList= MutableLiveData<Boolean>()
    val isEmptyList: LiveData<Boolean> = _isEmptyList

    private val _booksDb = MutableLiveData<List<Book>>()
    val booksDb: LiveData<List<Book>> = _booksDb

    fun getBooksOnDb() {
        GlobalScope.launch {
            _booksDb.postValue(repository.getBooksOnDB())
        }
    }

    fun getBooks(){
        _isViewLoading.postValue(true)
        repository.retrieveBooks(object: OperationCallback {
            override fun onError(obj: Any?) {
                _isViewLoading.postValue(false)
                _onMessageError.postValue(obj)
            }

            override fun onSuccess(obj: Any?) {
                _isViewLoading.postValue(false)

                if(obj!=null && obj is List<*>){
                    if(obj.isEmpty())
                        _isEmptyList.postValue(true)
                    else
                        _books.value= obj as List<Book>
                }
            }
        })
    }
}