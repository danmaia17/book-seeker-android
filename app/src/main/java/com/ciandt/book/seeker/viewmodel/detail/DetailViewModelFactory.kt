package com.ciandt.book.seeker.viewmodel.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ciandt.book.seeker.repository.BookDataSource

class DetailViewModelFactory(private val repository: BookDataSource): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DetailViewModel(repository) as T
    }
}