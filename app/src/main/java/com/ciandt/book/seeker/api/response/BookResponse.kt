package com.ciandt.book.seeker.api.response

import com.ciandt.book.seeker.model.Book

data class BookResponse(val resultCount:Int?,val results:List<Book>?){
    fun isSuccess():Boolean= (results?.isNotEmpty()!!)
}