package com.ciandt.book.seeker.repository

import android.util.Log
import androidx.annotation.UiThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ciandt.book.seeker.App
import com.ciandt.book.seeker.api.Api
import com.ciandt.book.seeker.api.OperationCallback
import com.ciandt.book.seeker.api.response.BookResponse
import com.ciandt.book.seeker.database.AppDatabase
import com.ciandt.book.seeker.database.BookDao
import com.ciandt.book.seeker.model.Book
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BookRepository: BookDataSource {

    private var call: Call<BookResponse>? = null
    private var bd: AppDatabase? = null

    override fun retrieveBooks(callback: OperationCallback) {
        call = Api.build()?.books("kotlin", "ibook")
        bd = AppDatabase(App.applicationContext())

        call?.enqueue(object : Callback<BookResponse> {
            override fun onFailure(call: Call<BookResponse>, t: Throwable) {
                callback.onError(t.message)
            }

            override fun onResponse(call: Call<BookResponse>, response: Response<BookResponse>) {
                response.body()?.let {
                    if(response.isSuccessful && (it.isSuccess())){
                        Log.v("BookRepository", "data ${it.results}")

                        GlobalScope.launch {
                            for (b: Book in it.results!!) {
                                val book = Book(b.trackId,
                                    b.trackName,
                                    b.artistName,
                                    b.description,
                                    b.trackViewUrl,
                                    b.artworkUrl100)

                                bd?.bookDao()?.insert(book)
                            }
                        }

                        callback.onSuccess(it.results)
                    } else
                        callback.onError("error on BookRepository.onResponse")
                }
            }
        })
    }

    override suspend fun getBooksOnDB(): List<Book>? {
        return AppDatabase(App.applicationContext()).bookDao().getAllBooks()
    }

    override suspend fun getBookOnDB(trackId:Int): Book {
        return AppDatabase(App.applicationContext()).bookDao().getBookByTrackId(trackId)
    }

    override fun cancel() {
        call?.let {
            it.cancel()
        }
    }
}