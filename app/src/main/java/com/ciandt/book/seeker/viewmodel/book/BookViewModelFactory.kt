package com.ciandt.book.seeker.viewmodel.book

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ciandt.book.seeker.repository.BookDataSource

class BookViewModelFactory(private val repository: BookDataSource): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BookViewModel(repository) as T
    }
}