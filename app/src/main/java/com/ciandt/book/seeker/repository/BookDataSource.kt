package com.ciandt.book.seeker.repository

import com.ciandt.book.seeker.api.OperationCallback
import com.ciandt.book.seeker.model.Book

interface BookDataSource {
    fun retrieveBooks(callback: OperationCallback)
    fun cancel()
    suspend fun getBooksOnDB() : List<Book>?
    suspend fun getBookOnDB(trackId:Int) : Book?
}