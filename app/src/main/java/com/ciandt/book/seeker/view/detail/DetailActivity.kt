package com.ciandt.book.seeker.view.detail

import android.R.*
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.databinding.ActivityDetailBinding
import com.ciandt.book.seeker.di.Injection
import com.ciandt.book.seeker.model.Book
import com.ciandt.book.seeker.viewmodel.detail.DetailViewModel
import com.ciandt.book.seeker.viewmodel.detail.DetailViewModelFactory

class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding
    private lateinit var viewModel: DetailViewModel
    private var trackId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)

        trackId = intent.getIntExtra("param_id", 0)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        initViewModel()
        initWebView()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, DetailViewModelFactory(Injection.providerRepository())).get(
            DetailViewModel::class.java)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.book.observe(this, renderBookOnDb)
        viewModel.url.observe(this, loadUrl)
        viewModel.progressVisibility.observe(this, configProgressVisibility)

        viewModel.getBookOnDb(trackId)
    }

    private fun initWebView() {
        binding.wvBook.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                viewModel.setProgressVisibility(View.VISIBLE)
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                viewModel.setProgressVisibility(View.GONE)
            }
        }
    }

    private val renderBookOnDb = Observer<Book> {
        val actionBar = supportActionBar
        actionBar!!.title = it.trackName

        viewModel.setUrl(it.trackViewUrl)
    }

    private val loadUrl = Observer<String> {
        binding.wvBook.loadUrl(it)
    }

    private val configProgressVisibility = Observer<Int> {
        viewModel.setProgressVisibility(it)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
