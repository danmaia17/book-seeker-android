package com.ciandt.book.seeker.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ciandt.book.seeker.model.Book

@Dao
interface BookDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(book: Book)

    @Query("SELECT * FROM book" )
    fun getAllBooks() : List<Book>

    @Query("SELECT * FROM book WHERE trackId = :trackId")
    fun getBookByTrackId(trackId: Int): Book

    @Query("DELETE FROM book")
    fun deleteBooks()

}