package com.ciandt.book.seeker.viewmodel.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ciandt.book.seeker.model.Book
import com.ciandt.book.seeker.repository.BookDataSource
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DetailViewModel(private val repository: BookDataSource) : ViewModel() {

    private val _book = MutableLiveData<Book>()
    val book: LiveData<Book> = _book

    private val _url = MutableLiveData<String>()
    val url: LiveData<String> = _url

    private val _progressVisibility = MutableLiveData<Int>()
    val progressVisibility: LiveData<Int> = _progressVisibility

    fun getBookOnDb(trackId:Int) {
        GlobalScope.launch {
            _book.postValue(repository.getBookOnDB(trackId))
        }
    }

    fun setUrl(u : String) {
        _url.postValue(u)
    }

    fun setProgressVisibility(visibility : Int) {
        _progressVisibility.postValue(visibility)
    }
}